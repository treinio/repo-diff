#!/bin/bash

repository_name="qtsdk"

while getopts "o:n:r:" optname
  do
    case "$optname" in
      "o")
        sha_old=$OPTARG
        ;;
      "n")
        sha_new=$OPTARG
        ;;
      "r")
        repository_name=$OPTARG
        ;;
      "?")
        echo "Unknown option $OPTARG" 1>&2 ; exit 1        
        ;;
      ":")
        echo "No argument value for option $OPTARG" 1>&2 ; exit 1
        ;;
      *)
      # Should not occur
        echo "Unknown error while processing options" ; exit 1
        ;;
    esac
  done

function check_initialized() {
    if [ ! -d ./$repository_name ]; then
        echo -e "Cannot find $PWD/$repository_name repository. Consider running this script elsewhere." 1>&2
        exit 1;
    fi
    
    cd $repository_name
    url=$(git config --get submodule.qtbase.url)    
    
    if [ -z "$url" ]; then
        cd ..
        echo "Error: $repository_name repository is not initialized." 1>&2
        exit 1;
    fi
    
    git pull --rebase
    git submodule update --recursive
    cd ..
}

function get_jira_summaries() {   
    for m in $modules
    do        
        if [ -f .$m-ids ]; then
            echo -e "\nIssues addressed for $m:"
            while read l ;
            do
                wget -q -O.tmpxml "https://bugreports.qt-project.org/sr/jira.issueviews:searchrequest-xml/temp/SearchRequest.xml?jqlQuery=key+%3D+$l&field=key&field=summary"
                summary=$(grep '<summary>' .tmpxml | sed -e 's/^ *//g;s/\&lt;/\</g;s/\&gt;/\>/g;s/\&amp;/\&/g;s/\&quot;/\"/g;s/\&apos;/\`/g')
                summary=${summary:9:${#summary}-19}                
                echo "    $l $summary"
            done < .$m-ids
        fi
    done
    rm -f .tmpxml
}

function get_submodules() {
    local mods
    cd $repository_name
    tmpfile=".submodules~"
    git submodule > ../$tmpfile
    while read l ;
    do
        sm=$(echo "$l" | awk '{ print $2 }') 
        if [ -d $sm ]; then                
            mods+=($sm)
        fi
    done < ../$tmpfile
    cd ..
    rm -f $tmpfile
    echo "${mods[@]}"
}

check_initialized

if [ -z "$sha_new" ]; then
    read -p "New sha1 [Or press ENTER for current HEAD]: " sha_new
    sha_new=${sha_new:-HEAD}
fi

if [ -z "$sha_old" ]; then
    read -p "Old sha1 [Or press ENTER for current HEAD]: " sha_old
    sha_old=${sha_old:-HEAD}
fi

echo -e "Using old sha1: $sha_old\nUsing new sha1: $sha_new"

modules=$(get_submodules)

cd $repository_name
for m in $modules
do
    echo -n "$m "
    _old=$(git ls-tree $sha_old $m | awk '{ print $3 }')
    _new=$(git ls-tree $sha_new $m | awk '{ print $3 }')
    if [ "$_old" == "$_new" ]; then
        echo "(no change)"
        rm -f ../.$m-ids
        continue
    fi
    cd $m
    git log $_old..$_new --pretty=%b | grep 'Task-number:' | awk '{ print $2 }' | sort -u > ../../.$m-ids
    cd ..
    echo "has changed, $(wc -l ../.$m-ids | cut -d ' ' -f 1) bug ID(s) mentioned in commit messages."
done

echo "Commits related to packaging (release-tools):"
git log $sha_old..$sha_new --pretty=oneline -- release-tools/*
cd ..

get_jira_summaries

# eof